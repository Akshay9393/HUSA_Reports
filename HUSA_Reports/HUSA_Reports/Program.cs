﻿using System;
using System.Linq;
using System.Data;
using System.IO;
using System.Collections.Generic;
using static HUSA_Reports.LDAP;
using System.Configuration;
using System.Threading;
using System.Diagnostics;
using System.Collections;
using System.Text;

namespace HUSA_Reports
{
    class Program
    {

        private void ExportDataSetToExcel(DataSet ds)
        {
            //Creae an Excel application instance
            Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();

            //Create an Excel workbook instance and open it from the predefined location
            Microsoft.Office.Interop.Excel.Workbook excelWorkBook = excelApp.Workbooks.Open(@"E:\Org.xlsx");

            foreach (DataTable table in ds.Tables)
            {
                //Add a new worksheet to workbook with the Datatable name
                Microsoft.Office.Interop.Excel.Worksheet excelWorkSheet = excelWorkBook.Sheets.Add();
                excelWorkSheet.Name = table.TableName;

                for (int i = 1; i < table.Columns.Count + 1; i++)
                {
                    excelWorkSheet.Cells[1, i] = table.Columns[i - 1].ColumnName;
                }

                for (int j = 0; j < table.Rows.Count; j++)
                {
                    for (int k = 0; k < table.Columns.Count; k++)
                    {
                        excelWorkSheet.Cells[j + 2, k + 1] = table.Rows[j].ItemArray[k].ToString();
                    }
                }
            }

            excelWorkBook.Save();
            excelWorkBook.Close();
            excelApp.Quit();

        }
        //Config file Variables.
        ArrayList arr = new ArrayList();

        static int iPageNo = 0;

        static int iLineNo = 0;

        static string pageBreak = System.Configuration.ConfigurationSettings.AppSettings["PageBreak"];

        static string mailing = System.Configuration.ConfigurationSettings.AppSettings["Mailing"];

        static string fromMailID = System.Configuration.ConfigurationSettings.AppSettings["FromMailId"];

        static string fromMailPwd = System.Configuration.ConfigurationSettings.AppSettings["FromMailPwd"];

        //roles variable created to refer CONFIG file for ROLES - Ramesh and Akshay - 17May2019
        static string roles = System.Configuration.ConfigurationSettings.AppSettings["ROLES"];

        static string strReportTitle = string.Empty;

        static string strReport = string.Empty;

        static string countryName = System.Configuration.ConfigurationSettings.AppSettings["CountryName"];

        static LDAP objLDAP = new LDAP();

        static string format = System.Configuration.ConfigurationSettings.AppSettings["OutputFormat"];

        private static string auditMonths = System.Configuration.ConfigurationSettings.AppSettings["AuditMonths"];

        private static object DataTableCollection;
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("HUSA_Report HAS Started Executing");
                if (args.Length == 0)
                {
                    //ArmStrong();
                    strReport = "all";
                }
                else
                {
                    strReport = args[0];
                }
                switch (strReport)   // using switch case all to run the report modules
                {
                    case "all":

                        strReport = "VP01";
                        GenerateSecurityMatrix();

                        strReport = "VP02";
                        iPageNo = iLineNo = 0;
                        GenerateAuditTrailReport();

                        strReport = "VP03";
                        iPageNo = iLineNo = 0;
                        GenerateUserListingNew();

                        strReport = "VP04";
                        iPageNo = iLineNo = 0;
                        // GenerateAccessViolation();

                        strReport = "VP05";
                        iPageNo = iLineNo = 0;
                        // GenerateDormantAccount();
                        break;

                    case "VP01":
                        GenerateSecurityMatrix();

                        break;

                    case "VP02":
                        GenerateAuditTrailReport();

                        break;


                    case "VP03":
                        GenerateUserListingNew();

                        break;

                    case "MailPassword":
                        Console.WriteLine("Enter Mail Password");
                        string password = Console.ReadLine();
                        string newPwd = Base64Encode(password);
                        changePasswordValueInAppConfig(newPwd, "FromMailPwd");
                        Console.WriteLine("Password configured sucessfully");
                        Thread.Sleep(5000);
                        Environment.Exit(0);
                        //GenerateAccessProfile();
                        //GenerateDormantAccount();
                        break;
                    case "DBPassword":
                        Console.WriteLine("Enter DB Password");
                        string dbPassword = Console.ReadLine();
                        string dbnewPwd = Base64Encode(dbPassword);
                        changePasswordValueInAppConfig(dbnewPwd, "DBPassword");
                        Console.WriteLine("DB Password configured sucessfully");
                        Thread.Sleep(5000);
                        Environment.Exit(0);
                        //GenerateAccessProfile();
                        //GenerateDormantAccount();
                        break;


                    case "LDAPPassword":
                        Console.WriteLine("Enter LDAP Password");
                        string ldppassword = Console.ReadLine();
                        string newldpPwd = Base64Encode(ldppassword);
                        changePasswordValueInAppConfig(newldpPwd, "LDAPPassword");
                        Console.WriteLine("Password configured sucessfully");
                        Thread.Sleep(5000);
                        Environment.Exit(0);
                        break;
                    case "VP04":

                        break;
                    case "VP05":
                        //GenerateUserListing();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                Log.MakeEntry(e.Message.ToString());
                var st = new StackTrace(e, true);
                // Get the top stack frame
                var frame = st.GetFrame(0);
                // Get the line number from the stack frame
                var line = frame.GetFileLineNumber();
                Log.MakeEntry(e.StackTrace);
                Log.MakeEntry(line.ToString());
            }
            finally
            {
                GC.Collect();
            }

        }
        #region Report Generate Modules

        private static void GenerateSecurityMatrix()
        {
            try
            {
                Console.WriteLine("Generating SecurityMatrix Report");
                Program.strReportTitle = "VocalPassword_Security_Matrix_Report";
                SortedDictionary<string, int> objSDTmp1 = new SortedDictionary<string, int>();
                DirectoryEntryLDAP directoryEntryLdap = new DirectoryEntryLDAP();
                objSDTmp1.Add("1Roles", 60);
                objSDTmp1.Add("2Operations", 40);
                TextWriter objTWTmp1 = (TextWriter)new StreamWriter(ConfigurationSettings.AppSettings["ResultFilePath"] + ConfigurationSettings.AppSettings["VP01"] + "_" + DateTime.Now.ToString("yyyyMMdd") + "." + Program.format ?? "");
                DAL dal = new DAL();
                DataTable dataTable1 = new DataTable();
                dataTable1.Columns.Add("Roles", typeof(string));
                dataTable1.Columns.Add("Operations", typeof(string));
                DataTable dataTable2 = new DataTable();
                DataTable dataTable3 = new DataTable();
                DataTable dataTable4 = new DataTable();
                DataTable dataTable5 = new DataTable();
                DataTable dataTable6 = new DataTable();
                DataTable dataTable7 = new DataTable();
                DataTable dataTable8 = new DataTable();
                DataTable dataTable9 = new DataTable();
                DataTable dataTable10 = new DataTable();
                DataSet dataSet = new DataSet();
                DataTable dataTable11 = new DataTable();
                DataTable dataTable12 = new DataTable();
                DataTable dataTable13 = new DataTable();
                //SQL query modified to display roles created under System Level. Added -1 in scopeID -Ramesh & Akshay 14/05/2019
                //Commented the DataTable dataTableFromQuery1 to refer CONFIG file for ROLES - Ramesh and Akshay - 17May2019
                //DataTable dataTableFromQuery1 = dal.GenerateDataTableFromQuery("select distinct name from nvb_az_roles where scope_id in ('" + Program.scopeID + "', '-1')", false);

                //SQL query to get rolename from CONFIG file - Ramesh and Akshay - 17May2019
                DataTable dataTableFromQuery1 = dal.GenerateDataTableFromQuery("select distinct name from nvb_az_roles where name in (" + Program.roles + ")", false);
                if (dataTableFromQuery1 != null)
                {
                    try
                    {
                        foreach (DataRow row1 in (InternalDataCollectionBase)dataTableFromQuery1.Rows)
                        {
                            try
                            {
                                //SQL query modified to display roles created under System Level. Added -1 in scopeID -Ramesh & Akshay 14/05/2019
                                //Commented the  DataTable dataTableFromQuery2 to refer role variable to display Operation Name - Ramesh and Akshay - 17May2019
                                // DataTable dataTableFromQuery2 = dal.GenerateDataTableFromQuery("select nao.NAME as column1 from nvb_az_roles nar inner join nvb_az_operation_role naor on nar.ROLE_ID=naor.ROLE_ID inner join nvb_az_operations nao on naor.OPERATION_ID=nao.OPERATION_ID where nar.scope_id in ('" + Program.scopeID + "', '-1') and nar.Name='" + row1[0].ToString() + "'", false);

                                //SQL query to get Operation Name for roles variable - Ramesh and Akshay - 17May2019
                                DataTable dataTableFromQuery2 = dal.GenerateDataTableFromQuery("select nao.NAME as column1 from nvb_az_roles nar inner join nvb_az_operation_role naor on nar.ROLE_ID=naor.ROLE_ID inner join nvb_az_operations nao on naor.OPERATION_ID=nao.OPERATION_ID where nar.Name='" + row1[0].ToString() + "'", false);
                                int num = 1;
                                foreach (DataRow row2 in (InternalDataCollectionBase)dataTableFromQuery2.Rows)
                                {
                                    try
                                    {
                                        if (num == 1)
                                        {
                                            try
                                            {
                                                if (row2[0] != null)
                                                {
                                                    DataRow row3 = dataTable1.NewRow();
                                                    row3[0] = (object)row1[0].ToString();
                                                    row3[1] = (object)row2[0].ToString();
                                                    dataTable1.Rows.Add(row3);
                                                    num = 0;
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Log.MakeEntry(ex.Message.ToString());
                                            }
                                        }
                                        else
                                        {
                                            try
                                            {
                                                if (row2[0] != null)
                                                {
                                                    DataRow row3 = dataTable1.NewRow();
                                                    row3[0] = (object)" ";                      //Excel Printing Function(-)
                                                    row3[1] = (object)row2[0].ToString();
                                                    dataTable1.Rows.Add(row3);
                                                    
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                Log.MakeEntry(ex.Message.ToString());
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.MakeEntry(ex.Message.ToString());
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.MakeEntry(ex.Message.ToString());
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.MakeEntry(ex.Message);
                        Log.MakeEntry(ex.StackTrace);
                    }
                }
                if (dataTable1 != null)
                {
                    string str1 = "";
                    for (int index = 0; index <= dataTable1.Rows.Count; ++index)
                    {
                        if (str1 == "" || str1.ToUpper() != dataTable1.Rows[index][0].ToString().ToUpper())
                        {
                            try
                            {
                                Program.WriteReport(objTWTmp1, dataTable1.Rows[index][0].ToString().PadRight(int.Parse(objSDTmp1["1Roles"].ToString())) + " " + dataTable1.Rows[index][1].ToString().PadRight(int.Parse(objSDTmp1["2Operations"].ToString())), Program.strReportTitle, objSDTmp1);
                            }
                            catch (Exception ex)
                            {
                                Log.MakeEntry(ex.Message.ToString());
                            }
                        }
                        else
                        {
                            int count = dataTable1.Rows.Count;
                            Log.MakeEntry(count.ToString());
                            try
                            {
                                TextWriter objTWTmp2 = objTWTmp1;
                                string str2 = dataTable1.Rows[index][0].ToString();
                                count = objSDTmp1["1Roles"];
                                int totalWidth1 = int.Parse(count.ToString());
                                string str3 = str2.PadRight(totalWidth1);
                                string str4 = dataTable1.Rows[index][1].ToString();
                                count = objSDTmp1["2Operations"];
                                int totalWidth2 = int.Parse(count.ToString());
                                string str5 = str4.PadRight(totalWidth2);
                                string strContent = str3 + " " + str5;
                                string strReportTitle = Program.strReportTitle;
                                SortedDictionary<string, int> objSDTmp2 = objSDTmp1;
                                Program.WriteReport(objTWTmp2, strContent, strReportTitle, objSDTmp2);
                            }
                            catch (Exception ex)
                            {
                                Log.MakeEntry(ex.Message.ToString());
                            }
                        }
                    }
                }
                if (Program.iLineNo == 0)
                    Program.WriteReport(objTWTmp1, "Empty", Program.strReportTitle, objSDTmp1);
                Program.WriteReportEnd(objTWTmp1);
                Console.WriteLine("Security MAtrix Has Been Generated");
                objTWTmp1.Close();
            }
            catch (Exception ex)
            {
                Log.MakeEntry(ex.Message);
                int fileLineNumber = new StackTrace(ex, true).GetFrame(0).GetFileLineNumber();
                Log.MakeEntry(ex.StackTrace);
                Log.MakeEntry(fileLineNumber.ToString());

            }
        }

        static void writeReportForRoles(string firstVal, string secVal)
        {

        }
        
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        private static void GenerateAuditTrailReport()
        {
            try
            {
                Console.WriteLine("Generating AuditTrailReport");
                Program.strReportTitle = "VocalPassword_AuditTrail_Report";
                Log.MakeEntry(Program.strReportTitle);
                SortedDictionary<string, int> objSDTmp1 = new SortedDictionary<string, int>();
                objSDTmp1.Add("1Changed_By_User", 20);
                objSDTmp1.Add("2Action", 7);
                objSDTmp1.Add("3Action_Date", 20);
                objSDTmp1.Add("4User", 20);
                objSDTmp1.Add("5Role", 25);
                objSDTmp1.Add("6Old_Value", 10);
                objSDTmp1.Add("7New_Value", 10);
                TextWriter objTWTmp1 = (TextWriter)new StreamWriter(ConfigurationSettings.AppSettings["ResultFilePath"] + ConfigurationSettings.AppSettings["VP02"] + "_" + DateTime.Now.ToString("yyyyMMdd") + "." + Program.format ?? "");
                DAL dal = new DAL();
                DataTable dataTableFromQuery = dal.GenerateDataTableFromQuery("SELECT USER_NAME, PARAM2 AS LANID, PARAM3 AS ROLE, METHOD_NAME, TO_CHAR(CREATION_TIMESTAMP,'yyyy-mm-dd_hh24:mi:ss') Dates FROM NVB_REQUEST_CONTEXT WHERE METHOD_NAME IN('AssignUserOrGroupToRole', 'RemoveUserOrGroupFromRole') AND CREATION_TIMESTAMP >= TO_CHAR(add_months(sysdate," + Program.auditMonths + "),'DD-MON-YYYY') ORDER BY Dates asc", false);

                if (dataTableFromQuery != null)
                {
                    Log.MakeEntry(Convert.ToString(dataTableFromQuery.Rows.Count));
                    string empty1 = string.Empty;
                    string empty2 = string.Empty;
                    string str1 = string.Empty;
                    string empty3 = string.Empty;
                    string strLog = "UPDATE";
                    for (int index = 0; index < dataTableFromQuery.Rows.Count; ++index)
                    {
                        Log.MakeEntry(dataTableFromQuery.Rows[index][0].ToString());
                        int num1;
                        if (empty3 == "" || empty3.ToUpper() != dataTableFromQuery.Rows[index][1].ToString().ToUpper() || empty2.ToUpper() != dataTableFromQuery.Rows[index][0].ToString().ToUpper())
                        {
                            Log.MakeEntry("if");
                            strLog = "UPDATE";
                            str1 = dataTableFromQuery.Rows[index][0].ToString();
                            if (str1.Contains("\\"))
                                str1 = str1.Substring(str1.IndexOf('\\') + 1);
                            int num2 = int.Parse("0" + dal.GetRequiredData("SELECT to_char(COUNT(*)) AS CURRENT_DATE_COUNT FROM NVB_AZ_AUTHORIZATION  WHERE WINDOWS_IDENTITY='" + dataTableFromQuery.Rows[index][1].ToString() + "'", false));
                            int num3 = int.Parse("0" + dal.GetRequiredData("SELECT to_char(COUNT(*)) AS CURRENT_DATE_COUNT FROM NVB_AZ_AUTHORIZATION  where CREATION_TIMESTAMP >= TO_CHAR(add_months(sysdate," + Program.auditMonths + "),'DD-MON-YYYY') AND WINDOWS_IDENTITY='" + dataTableFromQuery.Rows[index][1].ToString() + "'", false));
                            Log.MakeEntry("After second query. iTotalRolesOnDate" + num3.ToString());
                            Log.MakeEntry("ITotalRoles" + num2.ToString());
                            Log.MakeEntry("ITotalRolesOnDate " + num3.ToString());
                            if (num2 == 0)
                            {
                                Log.MakeEntry("Entered Delete");
                                strLog = "DELETE";
                            }
                            else if (num2 == num3)
                            {
                                strLog = "ADD";
                                Log.MakeEntry(strLog);
                            }
                            Log.MakeEntry(strLog);
                            TextWriter objTWTmp2 = objTWTmp1;
                            string[] strArray = new string[14];
                            string upper = str1.ToUpper();
                            num1 = objSDTmp1["1Changed_By_User"];
                            int totalWidth1 = int.Parse(num1.ToString());
                            strArray[0] = upper.PadRight(totalWidth1);
                            strArray[1] = "  ";
                            string str2 = strLog;
                            num1 = objSDTmp1["2Action"];
                            int totalWidth2 = int.Parse(num1.ToString());
                            strArray[2] = str2.PadRight(totalWidth2);
                            strArray[3] = "  ";
                            string str3 = dataTableFromQuery.Rows[index][4].ToString();
                            num1 = objSDTmp1["3Action_Date"];
                            int totalWidth3 = int.Parse(num1.ToString());
                            strArray[4] = str3.PadRight(totalWidth3);
                            strArray[5] = "  ";
                            string str4 = dataTableFromQuery.Rows[index][1].ToString();
                            num1 = objSDTmp1["4User"];
                            int totalWidth4 = int.Parse(num1.ToString());
                            strArray[6] = str4.PadRight(totalWidth4);
                            strArray[7] = "  ";
                            string str5 = dataTableFromQuery.Rows[index][2].ToString();
                            num1 = objSDTmp1["5Role"];
                            int totalWidth5 = int.Parse(num1.ToString());
                            strArray[8] = str5.PadRight(totalWidth5);
                            strArray[9] = "  ";
                            string str6 = dataTableFromQuery.Rows[index][3].ToString() == "AssignUserOrGroupToRole" ? "No" : "Yes";
                            num1 = objSDTmp1["6Old_Value"];
                            int totalWidth6 = int.Parse(num1.ToString());
                            strArray[10] = str6.PadRight(totalWidth6);
                            strArray[11] = "  ";
                            string str7 = dataTableFromQuery.Rows[index][3].ToString() == "AssignUserOrGroupToRole" ? "Yes" : "No";
                            num1 = objSDTmp1["7New_Value"];
                            int totalWidth7 = int.Parse(num1.ToString());
                            strArray[12] = str7.PadRight(totalWidth7);
                            strArray[13] = "  ";
                            string strContent = string.Concat(strArray);
                            string strReportTitle = Program.strReportTitle;
                            SortedDictionary<string, int> objSDTmp2 = objSDTmp1;
                            Program.WriteReport(objTWTmp2, strContent, strReportTitle, objSDTmp2);
                            empty3 = dataTableFromQuery.Rows[index][1].ToString();
                            empty2 = dataTableFromQuery.Rows[index][0].ToString();
                        }
                        else
                        {
                            TextWriter objTWTmp2 = objTWTmp1;
                            string[] strArray = new string[14];
                            string upper = str1.ToUpper();
                            num1 = objSDTmp1["1Changed_By_User"];
                            int totalWidth1 = int.Parse(num1.ToString());
                            strArray[0] = upper.PadRight(totalWidth1);
                            strArray[1] = "  ";
                            string str2 = strLog;
                            num1 = objSDTmp1["2Action"];
                            int totalWidth2 = int.Parse(num1.ToString());
                            strArray[2] = str2.PadRight(totalWidth2);
                            strArray[3] = "  ";
                            string str3 = dataTableFromQuery.Rows[index][4].ToString();
                            num1 = objSDTmp1["3Action_Date"];
                            int totalWidth3 = int.Parse(num1.ToString());
                            strArray[4] = str3.PadRight(totalWidth3);
                            strArray[5] = "  ";
                            string str4 = dataTableFromQuery.Rows[index][1].ToString();
                            num1 = objSDTmp1["4User"];
                            int totalWidth4 = int.Parse(num1.ToString());
                            strArray[6] = str4.PadRight(totalWidth4);
                            strArray[7] = "  ";
                            string str5 = dataTableFromQuery.Rows[index][2].ToString();
                            num1 = objSDTmp1["5Role"];
                            int totalWidth5 = int.Parse(num1.ToString());
                            strArray[8] = str5.PadRight(totalWidth5);
                            strArray[9] = "  ";
                            string str6 = dataTableFromQuery.Rows[index][3].ToString() == "AssignUserOrGroupToRole" ? "No" : "Yes";
                            num1 = objSDTmp1["6Old_Value"];
                            int totalWidth6 = int.Parse(num1.ToString());
                            strArray[10] = str6.PadRight(totalWidth6);
                            strArray[11] = "  ";
                            string str7 = dataTableFromQuery.Rows[index][3].ToString() == "AssignUserOrGroupToRole" ? "Yes" : "No";
                            num1 = objSDTmp1["7New_Value"];
                            int totalWidth7 = int.Parse(num1.ToString());
                            strArray[12] = str7.PadRight(totalWidth7);
                            strArray[13] = "  ";
                            string strContent = string.Concat(strArray);
                            string strReportTitle = Program.strReportTitle;
                            SortedDictionary<string, int> objSDTmp2 = objSDTmp1;
                            Program.WriteReport(objTWTmp2, strContent, strReportTitle, objSDTmp2);
                        }
                        if (Program.mailing == "true")
                            Program.sendMail(empty1);
                    }
                }
                if (Program.iLineNo == 0)
                    Program.WriteReport(objTWTmp1, "Empty", Program.strReportTitle, objSDTmp1);
                Program.WriteReportEnd(objTWTmp1);
                Console.WriteLine("AuditTrail Report Has Been Generated");
                objTWTmp1.Close();
            }
            catch (Exception ex)
            {
                Log.MakeEntry(ex.Message.ToString());
                Log.MakeEntry(ex.StackTrace);
                
            }
        }

        public static void changePasswordValueInAppConfig(string encodedPassword, string appconfigValue)
        {
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings[appconfigValue].Value = encodedPassword;
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
            }
            catch (Exception ex)
            {
                Log.MakeEntry(ex.Message);
                Log.MakeEntry(ex.StackTrace);

            }
            finally
            {

            }
        }

        static void sendMail(string toAddressMail)
        {
            Mailing mm = new Mailing();
            mm.SendfromOfficeMail(fromMailID, fromMailPwd, toAddressMail);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        private static void GenerateUserListingNew()
        {
            try
            {
                Console.WriteLine("Generating UserListing Report");
                Program.strReportTitle = "VocalPassword_UserListing_Report";
                SortedDictionary<string, int> objSDTmp1 = new SortedDictionary<string, int>();
                DirectoryEntryLDAP directoryEntryLdap = new DirectoryEntryLDAP();
                objSDTmp1.Add("1USER_NAME", 20);
                objSDTmp1.Add("2ROLE_NAME", 25);
                objSDTmp1.Add("3LastLogin", 30);
                objSDTmp1.Add("4IDStatus", 25);
                TextWriter objTWTmp1 = (TextWriter)new StreamWriter(ConfigurationSettings.AppSettings["ResultFilePath"] + ConfigurationSettings.AppSettings["VP03"] + "_" + DateTime.Now.ToString("yyyyMMdd") + "." + Program.format ?? "");
                DAL dal = new DAL();
                DataTable dataTable = new DataTable();
                dataTable.Columns.Add("USER_NAME", typeof(string));
                dataTable.Columns.Add("ROLE_NAME", typeof(string));
                dataTable.Columns.Add("LastLogin", typeof(string));
                dataTable.Columns.Add("IDStatus", typeof(string));
                Log.MakeEntry(dataTable.Columns.Count.ToString());

                DataTable dataTableFromQuery1 = dal.GenerateDataTableFromQuery("select lanid,to_char(login_date,'yyyy-mm-dd_hh24:mi:ss') as LastLogin from (select lanid,login_date,rank() over (partition by lanid order by login_date desc) rnk from access_log ) where rnk=1", false);

                StringBuilder stringBuilder = new StringBuilder();

                if (dataTableFromQuery1 != null)
                {
                    Log.MakeEntry("AccessLoUniq has data" + dataTableFromQuery1.Rows.Count.ToString());
                    foreach (DataRow row1 in (InternalDataCollectionBase)dataTableFromQuery1.Rows)
                    {
                        string str = row1[0].ToString();
                        Log.MakeEntry("Value inside forloop" + str);
                        foreach (DataRow row2 in (InternalDataCollectionBase)dal.GenerateDataTableFromQuery("select distinct windows_identity as USER_NAME,AUTH_ROLES.NAME AS ROLE_NAME from nvb_az_authorization auth inner join nvb_az_roles auth_roles on auth.role_id=auth_roles.role_id where UPPER(auth.windows_identity) like UPPER('" + str + "') order by windows_identity", false).Rows)
                        {
                            DataRow row3 = dataTable.NewRow();
                            row3[0] = (object)row2[0].ToString();
                            row3[1] = (object)row2[1].ToString();
                            row3[2] = (object)row1[1].ToString();
                            row3[3] = (object)"-";
                            dataTable.Rows.Add(row3);

                        }
                        stringBuilder.Append("UPPER('" + str + "')");
                        stringBuilder.Append(',');
                    }
                    if (stringBuilder != null)
                    {

                        if (stringBuilder.Length > 0)
                            --stringBuilder.Length;
                        DataTable dataTableFromQuery2 = dal.GenerateDataTableFromQuery("select distinct windows_identity as USER_NAME,AUTH_ROLES.NAME AS ROLE_NAME from nvb_az_authorization auth inner join nvb_az_roles auth_roles on auth.role_id=auth_roles.role_id where UPPER(auth.windows_identity) not in (" + stringBuilder.ToString() + ") order by windows_identity", false);
                        Log.MakeEntry(dataTableFromQuery2.Rows.Count.ToString());
                        foreach (DataRow row1 in (InternalDataCollectionBase)dataTableFromQuery2.Rows)
                        {
                            DataRow row2 = dataTable.NewRow();
                            row2[0] = (object)row1[0].ToString();
                            row2[1] = (object)row1[1].ToString();
                            row2[2] = (object)"-";
                            row2[3] = (object)"-";
                            dataTable.Rows.Add(row2);
                        }
                    }
                    if (dataTable != null)
                    {

                        string str1 = "";
                        int num;
                        for (int index = 0; index < dataTable.Rows.Count; ++index)
                        {
                            if (str1 == "" || str1.ToUpper() != dataTable.Rows[index][0].ToString().ToUpper())
                            {
                                TextWriter objTWTmp2 = objTWTmp1;
                                string[] strArray = new string[7];
                                string str2 = dataTable.Rows[index][0].ToString();
                                num = objSDTmp1["1USER_NAME"];
                                int totalWidth1 = int.Parse(num.ToString());
                                strArray[0] = str2.PadRight(totalWidth1);
                                strArray[1] = "  ";
                                string str3 = dataTable.Rows[index][1].ToString();
                                num = objSDTmp1["2ROLE_NAME"];
                                int totalWidth2 = int.Parse(num.ToString());
                                strArray[2] = str3.PadRight(totalWidth2);
                                strArray[3] = " ";
                                string str4 = dataTable.Rows[index][2].ToString();
                                num = objSDTmp1["3LastLogin"];
                                int totalWidth3 = int.Parse(num.ToString());
                                strArray[4] = str4.PadRight(totalWidth3);
                                strArray[5] = " ";
                                num = objSDTmp1["4IDStatus"];
                                strArray[6] = "-".PadRight(int.Parse(num.ToString()));
                                string strContent = string.Concat(strArray);
                                string strReportTitle = Program.strReportTitle;
                                SortedDictionary<string, int> objSDTmp2 = objSDTmp1;
                                Program.WriteReport(objTWTmp2, strContent, strReportTitle, objSDTmp2);
                                str1 = dataTable.Rows[index][0].ToString();
                                
                            }
                            else
                            {
                                TextWriter objTWTmp2 = objTWTmp1;
                                string str2 = dataTable.Rows[index][0].ToString();
                                num = objSDTmp1["1USER_NAME"];
                                int totalWidth1 = int.Parse(num.ToString());
                                string str3 = str2.PadRight(totalWidth1);
                                string str4 = dataTable.Rows[index][1].ToString();
                                num = objSDTmp1["2ROLE_NAME"];
                                int totalWidth2 = int.Parse(num.ToString());
                                string str5 = str4.PadRight(totalWidth2);
                                string str6 = str3 + "  " + str5;
                                string str7 = dataTable.Rows[index][2].ToString();
                                num = objSDTmp1["3LastLogin"];
                                int totalWidth3 = int.Parse(num.ToString());
                                string str8 = str7.PadRight(totalWidth3);
                                string str9 = str6 + str8;
                                num = objSDTmp1["4IDStatus"];
                                string str10 = "-".PadRight(int.Parse(num.ToString()));
                                string strContent = str9 + str10;
                                string strReportTitle = Program.strReportTitle;
                                SortedDictionary<string, int> objSDTmp2 = objSDTmp1;
                                Program.WriteReport(objTWTmp2, strContent, strReportTitle, objSDTmp2);
                                
                            }
                        }
                    }
                }
                if (Program.iLineNo == 0)
                    Program.WriteReport(objTWTmp1, "Empty", Program.strReportTitle, objSDTmp1);
                Program.WriteReportEnd(objTWTmp1);
                objTWTmp1.Close();
                Console.WriteLine("UserListing Report Has Been Generated");
            }
            catch (Exception ex)
            {
                Log.MakeEntry(ex.Message.ToString());
                
            }
        }

        #endregion

        #region Writing Report File

        static void WriteReport(TextWriter objTWTmp, string strContent, string strRptHeader, SortedDictionary<string, int> objSDTmp)
        {
            if (iLineNo == 0 || (iLineNo % 20) == 0)
            {
                if (iLineNo > 0)
                {
                    objTWTmp.WriteLine();
                    string strTmp = string.Empty;
                    foreach (var vKeys in objSDTmp)
                    {
                        strTmp = strTmp + "-".PadRight(int.Parse(vKeys.Value.ToString()), '-');
                    }
                    objTWTmp.WriteLine(strTmp);
                }
                Program.WriteReportHeader(objTWTmp, strRptHeader, objSDTmp);
            }
            objTWTmp.WriteLine(strContent);
            //objTWTmp.WriteLine();
            iLineNo++;
        }

        static void WriteReportHeader(TextWriter objTWTmp, string strRptHeader, SortedDictionary<string, int> objSDTmp)
        {
            iPageNo++;
            objTWTmp.WriteLine();
            if (iPageNo != 1)
            {
                objTWTmp.WriteLine("");
            }
            objTWTmp.WriteLine("Country: " + countryName + " ");
            objTWTmp.WriteLine("REF: " + strReportTitle + " \t\t\t\tREPORT_RUN_DATE\t: " + DateTime.Now.ToString("yyyy-MM-dd_HH:mm:ss") + "\t\t\t Page: " + iPageNo.ToString());
            objTWTmp.WriteLine(strRptHeader.PadRight(36, ' ') + " \tREPORT_DATE\t: " + DateTime.Now.ToString("yyyy-MM-dd"));
            objTWTmp.WriteLine();
            string strTmp = "";
            foreach (var vKeys in objSDTmp)
            {
                strTmp = strTmp + vKeys.Key.ToString().Substring(1).PadRight(int.Parse(vKeys.Value.ToString())) + "  ";
            }
            objTWTmp.WriteLine(strTmp);
            strTmp = "";
            foreach (var vKeys in objSDTmp)
            {
                strTmp = strTmp + "-".PadRight(int.Parse(vKeys.Value.ToString()), '-') + "  ";
            }
            objTWTmp.WriteLine(strTmp);
        }

        static void WriteReportEnd(TextWriter objTWTmp)
        {
            objTWTmp.WriteLine();
            objTWTmp.WriteLine();
            objTWTmp.WriteLine("-".PadRight(40, '-') + "    END OF REPORT    " + "-".PadRight(40, '-'));
            objTWTmp.WriteLine();
        }

        public static DataTable findDuplicates(DataTable dt)
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("USER_NAME", typeof(string));
            dataTable.Columns.Add("LastLogin", typeof(string));
            dataTable.Columns.Add("ROLE_NAME", typeof(string));
            dataTable.Columns.Add("IDStatus", typeof(string));
            for (int index1 = 0; index1 < dt.Rows.Count; ++index1)
            {
                for (int index2 = index1 + 1; index2 < dt.Rows.Count; ++index2)
                {
                    if (dt.Rows[index1][0].Equals(dt.Rows[index2][0]) && dt.Rows[index1][2].Equals(dt.Rows[index2][2]))
                    {
                        DataRow row = dataTable.NewRow();
                        row[0] = (object)"";
                        row[1] = dt.Rows[index1][1];
                        row[2] = (object)"";
                        row[3] = (object)"-";
                        dataTable.Rows.Add(row);
                    }
                    else
                    {
                        DataRow row = dataTable.NewRow();
                        row[0] = dt.Rows[index1][0];
                        row[1] = dt.Rows[index1][1];
                        row[2] = dt.Rows[index1][2];
                        row[3] = (object)"-";
                        dataTable.Rows.Add(row);
                    }
                }
            }
            return dataTable;
        }
        #endregion
    }
}

