﻿using System;
using System.IO;

namespace HUSA_Reports
{
    static class Log
    {

        static StreamWriter objFile = null;

        private static bool CreateLogFile()
        {
            try
            {
                if (!Directory.Exists(System.IO.Directory.GetCurrentDirectory().ToString() + "\\Logs"))
                {
                    Directory.CreateDirectory(System.IO.Directory.GetCurrentDirectory().ToString() + "\\Logs");
                }

                if (!File.Exists(System.IO.Directory.GetCurrentDirectory().ToString() + "\\Logs\\" + "SCBTISOReportsLog" + "_" +
                                                                                                         DateTime.Now.ToString("yyyyMMdd") + ".Txt"))
                {
                    File.Create(System.IO.Directory.GetCurrentDirectory().ToString() + "\\Logs\\" + "SCBTISOReportsLog" + "_" +
                                                                                                         DateTime.Now.ToString("yyyyMMdd") + ".Txt").Close();
                }
                objFile = new StreamWriter(System.IO.Directory.GetCurrentDirectory().ToString() + "\\Logs\\" + "SCBTISOReportsLog" + "_" +
                                                                                                         DateTime.Now.ToString("yyyyMMdd") + ".Txt", true);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                Console.Read();
                return false;
            }
        }

        public static void MakeEntry(string strLog)
        {
            try
            {
                CreateLogFile();

                if (objFile != null)
                {
                    objFile.WriteLine(DateTime.Now.ToString() + " : " + strLog);
                    objFile.Close();
                    objFile.Dispose();
                    objFile = null;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
                Console.Read();
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}

