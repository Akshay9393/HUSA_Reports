﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.Protocols;
using System.Security.Cryptography.X509Certificates;

namespace HUSA_Reports
{
    class LDAP
    {
        static string ldapServer = System.Configuration.ConfigurationSettings.AppSettings["ldapServer"];
        static string ldapSearchDN = System.Configuration.ConfigurationSettings.AppSettings["ldapSearchDN"];
        private static bool debug1 = System.Configuration.ConfigurationSettings.AppSettings["Debug"].ToString() == "true" ? true : false;
        private static string txtUserID = System.Configuration.ConfigurationSettings.AppSettings["LDAPUserName"];
        private static string txtPassword = System.Configuration.ConfigurationSettings.AppSettings["LDAPPassword"];

        static Dictionary<String, DirectoryEntryLDAP> searchList = new Dictionary<String, DirectoryEntryLDAP>();

        static System.Net.NetworkCredential cred = null;

        public bool ServerCallback(LdapConnection connection, X509Certificate certificate)
        {
            return true;
        }

        public LDAPErrors SearchUser(string userID, out DirectoryEntryLDAP directoryList)
        {
            if (debug1 == true)
            {
                Log.MakeEntry("user id is " + userID);
            }
            LDAPErrors retValue = LDAPErrors.Error;
            if (searchList.ContainsKey(userID.ToUpper()))
            {
                if (debug1 == true)
                {
                    Log.MakeEntry("Entry already exist");
                }
                bool ret = searchList.TryGetValue(userID.ToUpper(), out directoryList);
                return LDAPErrors.Success;
            }

            string filter = "(|(mail=" + userID + ")(displayName=" + userID + ")(name=" + userID + ")(sn=" + userID + ")(cn=" + userID + ")(sAMAccountName=" + userID + ")(givenName=" + userID + "))";
            AuthType auth = AuthType.Basic;

            if ((txtUserID != null && txtUserID != "") && (txtPassword != null && txtPassword != ""))
            {
                if (debug1 == true)
                {
                    Log.MakeEntry("domain user id is " + txtUserID + " " + GetString(Convert.FromBase64String(txtPassword)));
                }
                string pwd = GetString(Convert.FromBase64String(txtPassword));
                cred = new System.Net.NetworkCredential(txtUserID, pwd);
                retValue = search(filter, ldapSearchDN, ldapServer, cred, out directoryList, auth, false);
            }
            else
            {
                retValue = search(filter, ldapSearchDN, ldapServer, null, out directoryList, auth, false);
            }
            return retValue;
        }

        //LDAP Search
        LDAPErrors search(string filter, string searchDN, string ldapServer, System.Net.NetworkCredential cred, out DirectoryEntryLDAP directory, AuthType authType, bool useSSL)
        {
            string server = ldapServer;
            directory = new DirectoryEntryLDAP();
            LdapConnection con = new System.DirectoryServices.Protocols.LdapConnection(new LdapDirectoryIdentifier(server));
            con.SessionOptions.SecureSocketLayer = useSSL;
            con.SessionOptions.VerifyServerCertificate = new System.DirectoryServices.Protocols.VerifyServerCertificateCallback(ServerCallback);
            //if no credentials provided then use NTLM authentiaction (current user credentials)
            if (cred == null)
            {
                con.AuthType = AuthType.Ntlm;
            }
            else//if credentials are provided then use Basic auth type with provided credentials
            {
                con.Credential = cred;
                con.AuthType = authType;
            }
            using (con)
            {
                try
                {
                    //bind to LDAP    
                    con.Bind();
                    if (debug1 == true)
                    {
                        Log.MakeEntry("bind successful");
                    }

                }
                catch (LdapException ex)
                {
                    //Log.MakeEntry(ex);
                    Log.MakeEntry(ex.Message.ToString());

                    if (ex.Message.ToLower().Trim().Contains("supplied credential is invalid"))
                        return LDAPErrors.InvalidLogin;


                    if (ex.Message.ToLower().Trim().Contains("ldap server is unavailable"))
                        return LDAPErrors.InvalidServer;
                }
                catch (DirectoryOperationException exx)
                {
                    //Log.MakeEntry(exx);
                    Log.MakeEntry(exx.Message.ToString());

                    return LDAPErrors.BindError;
                }
                try
                {
                    //prepare the request
                    SearchRequest request = new SearchRequest(
                        searchDN,
                        filter,
                        System.DirectoryServices.Protocols.SearchScope.Subtree
                        );

                    request.SizeLimit = 2000;
                    //send request
                    SearchResponse response = (SearchResponse)con.SendRequest(request);
                    if (response.Entries.Count == 0)
                    {
                        if (debug1 == true)
                        {
                            Log.MakeEntry("No records");
                        }
                        return LDAPErrors.NoRecords;
                    }
                    else
                    {
                        //Log.MakeEntry("number of entries: " + response.Entries.Count);
                        //travers the result set and try to find requried attributes
                        foreach (SearchResultEntry entry in response.Entries)
                        {
                            DirectoryEntryLDAP ent = new DirectoryEntryLDAP();
                            ent.AllInfo = entry;
                            //Log.MakeEntry(entry.DistinguishedName);
                            //Log.MakeEntry(entry.Attributes);

                            SearchResultAttributeCollection attributes = entry.Attributes;
                            try
                            {
                                object[] vals = entry.Attributes["name"].GetValues(typeof(string));
                                if (vals.Length > 0)
                                {
                                    //Log.MakeEntry("name " + vals[0].ToString());
                                }
                            }
                            catch (Exception ex)
                            {
                                //Log.MakeEntry(ex);
                                Log.MakeEntry(ex.Message.ToString());
                            }
                            //department
                            try
                            {
                                object[] vals = entry.Attributes["department"].GetValues(typeof(string));
                                if (vals.Length > 0)
                                {
                                    if (debug1 == true)
                                    {
                                        Log.MakeEntry("Department " + vals[0].ToString());
                                    }
                                    ent.Department = vals[0].ToString();
                                }
                            }
                            catch (Exception ex)
                            {
                                //Log.MakeEntry(ex);
                                Log.MakeEntry(ex.Message.ToString());
                            }
                            //LANID
                            try
                            {

                                object[] vals = entry.Attributes["sAMAccountName"].GetValues(typeof(string));
                                if (vals.Length > 0)
                                {
                                    ent.LANID = vals[0].ToString();
                                }
                            }
                            catch (Exception ex)
                            {
                                //Log.MakeEntry(ex);
                                Log.MakeEntry(ex.Message.ToString());
                            }
                            //display name
                            try
                            {
                                object[] vals = entry.Attributes["displayName"].GetValues(typeof(string));
                                if (vals.Length > 0)
                                {
                                    if (debug1 == true)
                                    {
                                        Log.MakeEntry("DisplayName " + vals[0].ToString());
                                    }
                                    ent.DisplayName = vals[0].ToString();
                                }
                            }
                            catch (Exception ex)
                            {
                                //Log.MakeEntry(ex);
                                Log.MakeEntry(ex.Message.ToString());
                            }
                            bool nameCollected = false;
                            //first name
                            try
                            {
                                object[] vals = entry.Attributes["givenName"].GetValues(typeof(string));
                                if (vals.Length > 0)
                                {
                                    ent.GivenName = vals[0].ToString();
                                    nameCollected = true;
                                }
                            }
                            catch (Exception ex)
                            {
                                //Log.MakeEntry(ex);
                                Log.MakeEntry(ex.Message.ToString());
                            }
                            if (!nameCollected)
                            {
                                try
                                {
                                    object[] vals = entry.Attributes["name"].GetValues(typeof(string));
                                    if (vals.Length > 0)
                                    {
                                        ent.GivenName = vals[0].ToString();
                                        nameCollected = true;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    //Log.MakeEntry(ex);
                                    Log.MakeEntry(ex.Message.ToString());
                                }
                            }
                            //surname
                            try
                            {
                                object[] vals = entry.Attributes["sn"].GetValues(typeof(string));
                                if (vals.Length > 0)
                                {
                                    ent.Surname = vals[0].ToString();
                                }
                            }
                            catch (Exception ex)
                            {
                                //Log.MakeEntry(ex);
                                Log.MakeEntry(ex.Message.ToString());
                            }
                            //email addr
                            try
                            {
                                object[] vals = entry.Attributes["mail"].GetValues(typeof(string));
                                if (vals.Length > 0)
                                {
                                    ent.Mail = vals[0].ToString();
                                }
                            }
                            catch (Exception ex)
                            {
                                //Log.MakeEntry(ex);
                                Log.MakeEntry(ex.Message.ToString());
                            }
                            //company (department)
                            try
                            {
                                object[] vals = entry.Attributes["company"].GetValues(typeof(string));
                                if (vals.Length > 0)
                                {
                                    ent.Company = vals[0].ToString();
                                }
                            }
                            catch (Exception ex)
                            {
                                //Log.MakeEntry(ex);
                                Log.MakeEntry(ex.Message.ToString());
                            }
                            //telephone number
                            try
                            {
                                object[] vals = entry.Attributes["telephoneNumber"].GetValues(typeof(string));
                                if (vals.Length > 0)
                                {
                                    ent.TelephoneNumber = vals[0].ToString();
                                }
                            }
                            catch (Exception ex)
                            {
                                //Log.MakeEntry(ex);
                                Log.MakeEntry(ex.Message.ToString());
                            }

                            //title (designation)
                            try
                            {
                                object[] vals = entry.Attributes["title"].GetValues(typeof(string));
                                if (vals.Length > 0)
                                {
                                    ent.Title = vals[0].ToString();
                                }
                            }
                            catch (Exception ex)
                            {
                                //Log.MakeEntry(ex);
                                Log.MakeEntry(ex.Message.ToString());
                            }

                            directory = ent;
                        }
                        //Log.MakeEntry("LAN is is " + directory.LANID);
                        searchList.Add(directory.LANID.ToUpper(), directory);
                        return LDAPErrors.Success;
                    }
                }
                catch (DirectoryOperationException ex1)
                {
                    //Log.MakeEntry(ex1);
                    Log.MakeEntry(ex1.Message.ToString());
                    return LDAPErrors.InvalidDN;
                }
                catch (LdapException ex2)
                {
                    //Log.MakeEntry(ex2);
                    Log.MakeEntry(ex2.Message.ToString());
                    return LDAPErrors.InvalidFilter;
                }
                catch (Exception ex)
                {
                    //Log.MakeEntry(ex);
                    Log.MakeEntry(ex.Message.ToString());
                    return LDAPErrors.SearchError;
                }
            }
        }

        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        public enum LDAPErrors
        {
            Success,
            InvalidDN,
            InvalidFilter,
            Error,
            InvalidLogin,
            InvalidServer,
            BindError,
            NoRecords,
            SearchError
        }
    }

    class DirectoryEntryLDAP
    {
        internal SearchResultEntry AllInfo;
        internal string Company;
        internal string DisplayName;
        internal string GivenName;
        internal string LANID;
        internal string Mail;
        internal string Surname;
        internal string TelephoneNumber;
        internal string Title;
        internal string Department;
    }

}
