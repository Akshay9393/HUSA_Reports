﻿using Oracle.DataAccess.Client;
using System;
using System.Data;
using System.Data.SqlClient;

namespace HUSA_Reports
{
    class DAL
    {

        private OracleConnection objCon = null;

        private void ConnectionDB()
        {
            try
            {
                if (objCon == null)
                {
                    string strConnectionInfo = System.Configuration.ConfigurationSettings.AppSettings["ConnectionInfo"];

                    string strPassword = System.Configuration.ConfigurationSettings.AppSettings["DBPassword"];

                    strPassword = Base64Decode(strPassword);

                    strConnectionInfo = strConnectionInfo + " Password=" + strPassword + ";";

                    //Log.MakeEntry(strConnectionInfo);

                    objCon = new OracleConnection(strConnectionInfo);

                    Log.MakeEntry("Database Connection Done..!");

                    //Environment.Exit(0);
                }
                if (objCon.State == ConnectionState.Closed)
                {
                    objCon.Open();
                }

            }
            catch (Exception ex)
            {
                Log.MakeEntry(ex.Message.ToString());
            }

        }

        private void DissConnectionDB()
        {
            try
            {
                if (objCon.State == ConnectionState.Open)
                {
                    objCon.Close();
                    objCon.Dispose();
                }
            }
            catch (Exception ex)
            {
                Log.MakeEntry(ex.Message.ToString());
            }
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }


        public string GetRequiredData(string strSQL, bool blCloseConnection = false)
        {

            //Log.MakeEntry("DAL" + strSQL);
            try
            {
                this.ConnectionDB();

                var objCmd = new OracleCommand(strSQL);
                objCmd.Connection = objCon;
                objCmd.CommandType = CommandType.Text;

                OracleDataReader objDR;
                objDR = objCmd.ExecuteReader();

                string strValue = "";

                if (objDR.HasRows)
                {
                    while (objDR.Read())
                    {
                        if (!objDR.IsDBNull(0))
                        {
                            strValue = objDR.GetString(0);
                        }
                    }
                }

                objDR.Close();
                objDR.Dispose();
                //   Log.MakeEntry("Returned value is " + strValue);
                return strValue;

            }
            catch (Exception ex)
            {

                Log.MakeEntry(ex.Message.ToString());

                return null;
            }
            finally
            {
                if (blCloseConnection) this.DissConnectionDB();

            }
        }

        public DataTable GenerateDataTableFromQuery(string strSQL, bool blCloseConnection = false)
        {
            Log.MakeEntry("DataTable" + strSQL);
            try
            {
                this.ConnectionDB();
                OracleDataAdapter objDA = new OracleDataAdapter(strSQL, objCon);
                DataTable objDT = new DataTable();
                objDA.Fill(objDT);
                Log.MakeEntry(objDT.Rows.Count.ToString());
                return objDT;

            }
            catch (Exception ex)
            {
                Log.MakeEntry(ex.Message.ToString());
                return null;
            }
            finally
            {
                if (blCloseConnection) this.DissConnectionDB();
            }

        }
    }
}

